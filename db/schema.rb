# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_07_033544) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "bus_points", force: :cascade do |t|
    t.geography "lonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.datetime "started_at"
    t.datetime "ended_at"
    t.integer "reported_points"
    t.bigint "bus_id", null: false
    t.bigint "schedule_route_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bus_id"], name: "index_bus_points_on_bus_id"
    t.index ["schedule_route_id"], name: "index_bus_points_on_schedule_route_id"
  end

  create_table "buses", force: :cascade do |t|
    t.string "patent"
    t.string "gps_device"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "routes", force: :cascade do |t|
    t.string "name"
    t.geography "startlonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "endlonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "route_data", limit: {:srid=>4326, :type=>"st_polygon", :geographic=>true}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "schedule_routes", force: :cascade do |t|
    t.datetime "started_at"
    t.datetime "ended_at"
    t.string "state"
    t.bigint "route_id", null: false
    t.bigint "bus_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bus_id"], name: "index_schedule_routes_on_bus_id"
    t.index ["route_id"], name: "index_schedule_routes_on_route_id"
  end

  create_table "tour_events", force: :cascade do |t|
    t.string "description"
    t.string "category"
    t.bigint "bus_point_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bus_point_id"], name: "index_tour_events_on_bus_point_id"
  end

  add_foreign_key "bus_points", "buses"
  add_foreign_key "bus_points", "schedule_routes"
  add_foreign_key "schedule_routes", "buses"
  add_foreign_key "schedule_routes", "routes"
  add_foreign_key "tour_events", "bus_points"
end
