class CreateBusPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :bus_points do |t|
      t.st_point :lonlat, geographic: true
      t.datetime :started_at
      t.datetime :ended_at
      t.integer :reported_points
      t.belongs_to :bus, null: false, foreign_key: true
      t.belongs_to :schedule_route, null: false, foreign_key: true

      t.timestamps
    end
  end
end
