class CreateScheduleRoutes < ActiveRecord::Migration[6.0]
  def change
    create_table :schedule_routes do |t|
      t.datetime :started_at
      t.datetime :ended_at
      t.string :state
      t.belongs_to :route, null: false, foreign_key: true
      t.belongs_to :bus, null: false, foreign_key: true

      t.timestamps
    end
  end
end
