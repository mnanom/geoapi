class CreateTourEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :tour_events do |t|
      t.string :description
      t.string :category
      t.belongs_to :bus_point, null: false, foreign_key: true

      t.timestamps
    end
  end
end
