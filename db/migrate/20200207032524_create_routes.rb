class CreateRoutes < ActiveRecord::Migration[6.0]
  def change
    create_table :routes do |t|
      t.string :name
      t.st_point :startlonlat, geographic: true
      t.st_point :endlonlat, geographic: true
      t.st_polygon :route_data, geographic: true

      t.timestamps
    end
  end
end
