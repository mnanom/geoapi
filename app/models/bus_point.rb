class BusPoint < ApplicationRecord
  belongs_to :bus
  belongs_to :schedule_route
end
